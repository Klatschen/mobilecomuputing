package de.erfurt.kraba.krabaerfurt;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import de.erfurt.kraba.krabaerfurt.databse.AppDBHelper;
import de.erfurt.kraba.krabaerfurt.databse.AppDataSource;
import de.erfurt.kraba.krabaerfurt.datamodel.Block;
import de.erfurt.kraba.krabaerfurt.datamodel.Practice;
import de.erfurt.kraba.krabaerfurt.datamodel.Session;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SessionFragment.Communicator
{
    //-------------------------------------------------------------------------
    // const members
    //-------------------------------------------------------------------------
    private static String mc_FRAGMENT_TAG_BLOG = "blog";
    private static String mc_FRAGMENT_TAG_SESSIONS = "sessions";


    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    // override methods
    //-------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //-------------------------------------------------------------------------
        // If there is no SavedState, Fragment SessionFragment will be shown.
        // Otherwise, do nothing
        //-------------------------------------------------------------------------

        if(savedInstanceState == null)
        {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            SessionFragment frag = new SessionFragment();
            transaction.replace(R.id.main_frame, frag, mc_FRAGMENT_TAG_SESSIONS);
            transaction.commit();
        }
    }

    /**
     * Sets title of ActionBar and shows or hides the FAB
     * @param _title ActionBar Title
     */
    public void setActionBarTitle(String _title)
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(_title);
        }
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        } else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //-------------------------------------------------------------------------
        // Inflate the menu; this adds items to the action bar if it is present.
        //-------------------------------------------------------------------------
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_add_test)
        {
            AppDataSource dataSource = new AppDataSource(this);
            dataSource.open();

            try
            {
                //-------------------------------------------------------------------------
                // create and insert test data
                //-------------------------------------------------------------------------

                Practice practice = new Practice();
                List<Practice> practiceList = new ArrayList<>();

                for (int count = 1; count < 6; ++ count)
                {
                    practice = new Practice();
                    practice.setName("Übung " + String.valueOf(count));
                    practice.setRounds(String.valueOf(count) + " Wiederholungen");
                    practiceList.add(practice);
                }

                Block block = new Block();
                List<Block> blockList = new ArrayList<>();

                for (int count = 1; count < 6; ++ count)
                {
                    block = new Block();
                    block.setName("Block " + String.valueOf(count));
                    block.setTimeInMin("10");
                    block.setPractices(practiceList);
                    block.setTypeOfPractice("Aufwärmen");

                    blockList.add(block);
                }

                Session session = new Session();
                session.setDate("11.07.2016");
                session.setBlocks(blockList);

                int sessionID = 0;
                int blockID = 0;
                int practiceID = 0;

                sessionID = dataSource.insertSession(session);

                for(int count = 0; count < blockList.size(); ++ count)
                {
                    blockList.get(count).setSessionID(sessionID);
                    blockID = dataSource.insertBlock(blockList.get(count));
                }

                for(int count = 0; count < practiceList.size(); ++ count)
                {
                    practiceList.get(count).setBlockID(blockID);
                    practiceID = dataSource.insertPractice(practiceList.get(count));
                }
            }
            catch (Exception e)
            {
                Log.e("Testdata",e.getMessage());
            }
            finally
            {
                dataSource.close();

                //--------------------------------------------------------------------------------
                //Check if fragment is visible and reload it
                //--------------------------------------------------------------------------------
                Fragment frg = null;
                FragmentManager fragmentManager = getFragmentManager();
                frg = fragmentManager.findFragmentByTag(mc_FRAGMENT_TAG_SESSIONS);
                if(frg.isVisible())
                {
                    fragmentManager.beginTransaction()
                            .detach(frg)
                            .attach(frg)
                            .commit();
                }
            }
        }
        else if (id == R.id.action_delete_all)
        {
            try
            {
                AppDataSource dataSource = new AppDataSource(this);
                dataSource.open();
                dataSource.deleteAll();
                dataSource.close();
            }
            catch (Exception e)
            {
                Log.e(AppDBHelper.LOG_TAG,e.getMessage());
            }
            finally
            {
                //--------------------------------------------------------------------------------
                //Check if fragment is visible and reload it
                //--------------------------------------------------------------------------------
                Fragment frg = null;
                FragmentManager fragmentManager = getFragmentManager();
                frg = fragmentManager.findFragmentByTag(mc_FRAGMENT_TAG_SESSIONS);
                if(frg.isVisible())
                {
                    fragmentManager.beginTransaction()
                            .detach(frg)
                            .attach(frg)
                            .commit();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Fragment fragment = null;
        String tag = null;

        //-------------------------------------------------------------------------
        // Handle navigation view item clicks here.
        //-------------------------------------------------------------------------
        int id = item.getItemId();

        if (id == R.id.nav_training)
        {
            fragment = new SessionFragment();
            tag = mc_FRAGMENT_TAG_SESSIONS;
        }
        else if (id == R.id.nav_chat)
        {
            Intent i = new Intent(this,ChatActivity.class);
            startActivity(i);
        }
        else if (id == R.id.nav_faq)
        {

        }
        else if (id == R.id.nav_settings)
        {

        }
        else if (id == R.id.nav_blog_pospich)
        {
            fragment = new FragmentBlog();
            ((ApplicationKraba)getApplicationContext()).SetBlogID(0);
            tag = mc_FRAGMENT_TAG_BLOG;

        }
        else if (id == R.id.nav_blog_kraba)
        {
            fragment = new FragmentBlog();
            ((ApplicationKraba)getApplicationContext()).SetBlogID(1);
            tag = mc_FRAGMENT_TAG_BLOG;
        }

        if (fragment != null)
        {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.main_frame, fragment, tag).commit();
        }
        else
        {
            //-------------------------------------------------------------------------
            // error in creating fragment
            //-------------------------------------------------------------------------
            Log.e("MainActivity", "Error in creating fragment");
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void sendID(long _id)
    {
        ((ApplicationKraba)getApplicationContext()).SetSessionID((int)_id);
        Intent i = new Intent(this,BlockActivity.class);
        startActivity(i);
    }
}
