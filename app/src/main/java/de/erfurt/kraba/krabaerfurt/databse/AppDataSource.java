package de.erfurt.kraba.krabaerfurt.databse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.erfurt.kraba.krabaerfurt.datamodel.Block;
import de.erfurt.kraba.krabaerfurt.datamodel.Practice;
import de.erfurt.kraba.krabaerfurt.datamodel.Session;

public class AppDataSource
{
    //-------------------------------------------------------------------------
    // static members
    //-------------------------------------------------------------------------
    private static final String TAG = "DB";

    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------
    private SQLiteDatabase database;
    private AppDBHelper    dbHelper;

    private String[] SESSION_COLUMNS =
    {
            AppDBHelper.SESSIONS_ID,
            AppDBHelper.SESSIONS_DATE
    };

    private String[] BLOCKS_COLUMNS =
    {
            AppDBHelper.BLOCKS_ID,
            AppDBHelper.BLOCKS_PRACTICES,
            AppDBHelper.BLOCKS_TIMEINMIN,
            AppDBHelper.BLOCKS_NAME,
            AppDBHelper.BLOCKS_TYPEOFPRACTICE,
            AppDBHelper.BLOCKS_SESSION_ID
    };

    private String[] PRACTICES_COLUMNS =
    {
            AppDBHelper.PRACTICES_ID,
            AppDBHelper.PRACTICES_NAME,
            AppDBHelper.PRACTICES_ROUNDS,
            AppDBHelper.PRACTICES_BLOCK_ID
    };

    //-------------------------------------------------------------------------
    // public methods
    //-------------------------------------------------------------------------

    /**
     * Create CareManDBHelper
     * @param context Context
     */
    public AppDataSource(Context context)
    {
        dbHelper = new AppDBHelper(context);
    }

    /**
     * Gets reference to writable database
     */
    public void open()
    {
        Log.d(TAG, "open() wird ausgeführt...");
        database = dbHelper.getWritableDatabase();
        Log.d(TAG, "Referenz erhalten. Pfad zur DB: " + database.getPath());
    }

    /**
     * Close database reference
     */
    public void close()
    {
        dbHelper.close();
        Log.d(TAG, "Datenbank geschlossen.");
    }

    public void deleteAll()
    {
        dbHelper.deleteData(database);
        Log.d(TAG, "Daten gelöscht.");
    }

    /**
     * Delete and create new ChatTables
     */
    public void deleteChatDatabase()
    {
        dbHelper.deleteChatTables(database);
        dbHelper.createChatTables(database);
    }

    //-------------------------------------------------------------------------
    // public content methods
    //-------------------------------------------------------------------------

    /**
     * Insert a new session in DB.
     * @param _session Session to insert
     */
    public int insertSession(Session _session)
    {
        int result =  -1;
        try
        {
            ContentValues sessionValues = new ContentValues();

            sessionValues.put(AppDBHelper.SESSIONS_DATE,_session.getDate());

            result = (int)database.insert(AppDBHelper.TABLE_SESSIONS, null ,sessionValues);
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }

        return result;
    }

    public int insertBlock(Block _block)
    {
        int result = -1;
        try
        {
            ContentValues blockValues = new ContentValues();

            blockValues.put(AppDBHelper.BLOCKS_NAME,_block.getName());
            blockValues.put(AppDBHelper.BLOCKS_SESSION_ID,_block.getSessionID());
            blockValues.put(AppDBHelper.BLOCKS_TIMEINMIN,_block.getTimeInMin());
            blockValues.put(AppDBHelper.BLOCKS_TYPEOFPRACTICE,_block.getTypeOfPractice());

            result = (int)database.insert(AppDBHelper.TABLE_BLOCKS, null ,blockValues);
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }

        Log.i(TAG,"Block gespeichert, ID = " + String.valueOf(result) + " SessionID = " + String.valueOf(_block.getSessionID()));
        return result;
    }

    public int insertPractice(Practice _practice)
    {
        int result = -1;
        try
        {
            ContentValues values = new ContentValues();

            values.put(AppDBHelper.PRACTICES_NAME,_practice.getName());
            values.put(AppDBHelper.PRACTICES_BLOCK_ID,_practice.getBlockID());
            values.put(AppDBHelper.PRACTICES_ROUNDS,_practice.getRounds());

            result = (int)database.insert(AppDBHelper.TABLE_PRACTICES, null ,values);
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }

        Log.i(TAG,"Pracitce gespeichert, ID = " + String.valueOf(result) + " BlockID = " + String.valueOf(_practice.getBlockID()));
        return result;
    }


    //-------------------------------------------------------------------------
    // private content methods
    //-------------------------------------------------------------------------

    private Session cursorToSession(Cursor _cursor)
    {
        Session result = new Session();

        result.setID(_cursor.getInt(_cursor.getColumnIndex(AppDBHelper.SESSIONS_ID)));
        result.setDate(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.SESSIONS_DATE)));

        return result;
    }

    private Block cursorToBlock(Cursor _cursor)
    {
        Block result = new Block();

        result.setID(_cursor.getInt(_cursor.getColumnIndex(AppDBHelper.BLOCKS_ID)));
        result.setName(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.BLOCKS_NAME)));
        result.setTimeInMin(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.BLOCKS_TIMEINMIN)));
        result.setTypeOfPractice(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.BLOCKS_TYPEOFPRACTICE)));
        result.setSessionID(_cursor.getInt(_cursor.getColumnIndex(AppDBHelper.BLOCKS_SESSION_ID)));

        return result;
    }

    private Practice cursorToPractice(Cursor _cursor)
    {
        Practice result = new Practice();

        result.setID(_cursor.getInt(_cursor.getColumnIndex(AppDBHelper.PRACTICES_ID)));
        result.setName(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.PRACTICES_NAME)));
        result.setRounds(_cursor.getString(_cursor.getColumnIndex(AppDBHelper.PRACTICES_ROUNDS)));
        result.setBlockID(_cursor.getInt(_cursor.getColumnIndex(AppDBHelper.PRACTICES_BLOCK_ID)));

        return result;
    }

    public List<Block> getAllBlocksBySessionID(Integer _sessionID)
    {
        List<Block> result = new ArrayList<>();

        Cursor cursor = database.query(AppDBHelper.TABLE_BLOCKS,
                BLOCKS_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        Block item;

        while(!cursor.isAfterLast())
        {
            item = cursorToBlock(cursor);

            if(item.getSessionID() == _sessionID)
            {
                result.add(item);
            }

            cursor.moveToNext();
        }
        cursor.close();

        Log.i(TAG,String.valueOf(result.size()) + " Blocks aus DB gelesen");
        return result;
    }

    public List<Practice> getAllPracticesByBlockID(Integer _blockID)
    {
        List<Practice> result = new ArrayList<>();

        Cursor cursor = database.query(AppDBHelper.TABLE_PRACTICES,
                PRACTICES_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        Practice item;

        while(!cursor.isAfterLast())
        {
            item = cursorToPractice(cursor);

            if(item.getBlockID() == _blockID)
            {
                result.add(item);
            }

            cursor.moveToNext();
        }
        cursor.close();

        Log.i(TAG,String.valueOf(result.size()) + " Practices aus DB gelesen");
        return result;
    }

    public List<Session> getAllSessions()
    {
        List<Session> result = new ArrayList<>();

        Cursor cursor = database.query(AppDBHelper.TABLE_SESSIONS,
                SESSION_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        Session item;

        while(!cursor.isAfterLast())
        {
            item = cursorToSession(cursor);
            result.add(item);
            cursor.moveToNext();
        }
        cursor.close();

        return result;
    }


}
