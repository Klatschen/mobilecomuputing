package de.erfurt.kraba.krabaerfurt.datamodel;

import java.util.List;

public class Session
{
    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------
    private int      m_ID;
    private List<Block>  m_Blocks;
    private String       m_Date;

    //-------------------------------------------------------------------------
    // Getter
    //-------------------------------------------------------------------------

    public int getID()
    {
        return m_ID;
    }

    public List<Block> getBlocks()
    {
        return m_Blocks;
    }

    public String getDate()
    {
        return m_Date;
    }

    //-------------------------------------------------------------------------
    // Setter
    //-------------------------------------------------------------------------

    public void setID(int _id)
    {
        m_ID = _id;
    }

    public void setBlocks(List<Block> _blocks)
    {
        m_Blocks = _blocks;
    }

    public void setDate(String _date)
    {
        m_Date = _date;
    }
}
