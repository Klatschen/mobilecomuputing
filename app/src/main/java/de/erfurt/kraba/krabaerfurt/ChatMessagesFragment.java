package de.erfurt.kraba.krabaerfurt;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChatMessagesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat[] df = new DateFormat[]
            {
                DateFormat.getDateInstance(), DateFormat.getTimeInstance()
            };


    private OnFragmentInteractionListener mListener;
    private SimpleCursorAdapter adapter;
    private Date now;
    private Calendar m_Calendar;

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            mListener = (OnFragmentInteractionListener) context;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        now = new Date();
        m_Calendar = Calendar.getInstance();

        adapter = new SimpleCursorAdapter(getActivity(),
                R.layout.chat_message_row,
                null,
                new String[]{ChatDataProvider.COL_MSG, ChatDataProvider.COL_AT},
                new int[]{R.id.chat_text1, R.id.chat_text2},
                0);
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder()
        {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex)
            {
                switch (view.getId())
                {
                    case R.id.chat_text1:
                        LinearLayout root = (LinearLayout) view.getParent().getParent();
                        LinearLayout content = (LinearLayout) ((LinearLayout) view.getParent().getParent()).findViewById(R.id.chat_content_layout);
                        if (cursor.getString(cursor.getColumnIndex(ChatDataProvider.COL_SENT)).equals("device"))
                        {
                            root.setGravity(Gravity.RIGHT);
                            root.setPadding(50, 10, 10, 10);
                            content.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.message_bubble_out));
                        } else
                        {
                            root.setGravity(Gravity.LEFT);
                            root.setPadding(10, 10, 50, 10);
                            content.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.message_bubble_in));
                        }
                        break;
                    case R.id.chat_text2:
                        TextView timeText = (TextView) view;
                        timeText.setText(getDisplayTime(cursor.getString(columnIndex)));
                        return true;
                }
                return false;
            }
        });
        setListAdapter(adapter);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        Bundle args = new Bundle();
        args.putString(ChatDataProvider.COL_AT, mListener.getProfileEmail());
        getLoaderManager().initLoader(0, args, this);
        getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        getListView().setStackFromBottom(true);
        getListView().setBackgroundColor(Color.WHITE);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        CursorLoader loader = new CursorLoader(getContext(),
                ChatDataProvider.CONTENT_URI_MESSAGES,
                new String[]{ChatDataProvider.COL_ID, ChatDataProvider.COL_MSG,
                            ChatDataProvider.COL_AT, ChatDataProvider.COL_SENT},
                null,
                null,
                ChatDataProvider.COL_ID + " ASC");
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Log.d("CHAT", data.toString());
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        adapter.swapCursor(null);
    }

    public interface OnFragmentInteractionListener
    {
        public String getProfileEmail();
    }

    private String getDisplayTime(String datetime)
    {
        try
        {
            Calendar savedDate = Calendar.getInstance();
            savedDate.setTime(sdf.parse(datetime));

            if (m_Calendar.get(Calendar.YEAR)==savedDate.get(Calendar.YEAR) &&
                    m_Calendar.get(Calendar.MONTH)==savedDate.get(Calendar.MONTH) &&
                    m_Calendar.get(Calendar.DATE)==savedDate.get(Calendar.DATE))
            {
                return df[1].format(savedDate.getTime());
            }
            return df[0].format(savedDate.getTime());
        }
        catch (ParseException e)
        {
            return datetime;
        }
    }

}