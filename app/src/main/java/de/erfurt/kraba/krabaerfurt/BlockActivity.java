package de.erfurt.kraba.krabaerfurt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.erfurt.kraba.krabaerfurt.databse.AppDataSource;
import de.erfurt.kraba.krabaerfurt.datamodel.Block;

public class BlockActivity extends AppCompatActivity implements AdapterView.OnItemClickListener
{
    //-------------------------------------------------------------------------
    // private member
    //-------------------------------------------------------------------------
    AppDataSource    m_dataSource;
    BlockListAdapter m_adapter;
    ListView         m_lvBlocks;
    TextView         m_emptyText;
    int              m_sessionID;


    //-------------------------------------------------------------------------
    // override methods
    //-------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block);

        try
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception ex)
        {
            Log.e("Toolbar", ex.getMessage());
        }

        m_lvBlocks  = (ListView) findViewById(R.id.activity_block_lvBlocks);
        m_emptyText = (TextView) findViewById(R.id.activity_block_lvEmpty);

        m_lvBlocks.setOnItemClickListener(this);

        m_sessionID = ((ApplicationKraba)getApplicationContext()).GetSessionID();
        updateView(m_sessionID);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        TextView c = (TextView) view.findViewById(R.id.row_block_list_ID);
        String blockID = c.getText().toString();

        Intent i = new Intent(this, PracticeActivity.class);
        i.putExtra("blockID",Integer.valueOf(blockID));
        startActivity(i);
    }

    //-------------------------------------------------------------------------
    // content methods
    //-------------------------------------------------------------------------

    /**
     * Updates content.
     */
    public void updateView(int _sessionID)
    {
        m_dataSource = new AppDataSource(this);
        m_dataSource.open();

        List<Block> blockList = m_dataSource.getAllBlocksBySessionID(_sessionID);
        m_adapter = new BlockListAdapter(this, blockList);

        m_lvBlocks.setAdapter(m_adapter);
        m_lvBlocks.setEmptyView(m_emptyText);

        m_dataSource.close();
    }
}
