package de.erfurt.kraba.krabaerfurt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.erfurt.kraba.krabaerfurt.datamodel.Block;

public class BlockListAdapter extends ArrayAdapter<Block>
{
    //--------------------------------------------------------------------------------
    // member variables
    //--------------------------------------------------------------------------------
    private final Context context;
    private final List<Block> itemsArrayList;

    public BlockListAdapter(Context context, List<Block> itemsArrayList)
    {
        super(context, R.layout.row_block_list, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.row_block_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.ID   = (TextView) convertView.findViewById(R.id.row_block_list_ID);
            viewHolder.Name = (TextView) convertView.findViewById(R.id.row_block_list_name);
            viewHolder.Type = (TextView) convertView.findViewById(R.id.row_block_list_type);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.ID.setText(String.valueOf(itemsArrayList.get(position).getID()));
        viewHolder.Name.setText(itemsArrayList.get(position).getName());
        viewHolder.Type.setText(itemsArrayList.get(position).getTypeOfPractice());

        return convertView;
    }

    //--------------------------------------------------------------------------------
    // helper classes
    //--------------------------------------------------------------------------------

    static class ViewHolder
    {
        public TextView ID;
        public TextView Name;
        public TextView Type;
    }
}
