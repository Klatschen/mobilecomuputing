package de.erfurt.kraba.krabaerfurt;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChatActivity extends AppCompatActivity implements ChatMessagesFragment.OnFragmentInteractionListener
{
    //--------------------------------------------------------------------------------
    // static members
    //--------------------------------------------------------------------------------
    private static String TAG = "Chat";

    //--------------------------------------------------------------------------------
    // Member variables
    //--------------------------------------------------------------------------------
    private EditText msgEdit;
    private Button sendBtn;

    //--------------------------------------------------------------------------------
    // Override methods
    //--------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        msgEdit = (EditText) findViewById(R.id.chat_msg_edit);
        sendBtn = (Button) findViewById(R.id.chat_send_button);

        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sendBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    ContentValues values = new ContentValues(2);
                    values.put(ChatDataProvider.COL_MSG, msgEdit.getText().toString());
                    values.put(ChatDataProvider.COL_SENT, "device");
                    getContentResolver().insert(ChatDataProvider.CONTENT_URI_MESSAGES, values);

                    //RestCommunicator helper = new RestCommunicator(getApplicationContext());
                    //helper.sendTextMessage(getApplicationContext(), msgEdit.getText().toString());
                }
                catch (Exception e)
                {
                    Log.e(TAG,e.getMessage());
                    Toast.makeText(getApplicationContext(),"FEHLER",Toast.LENGTH_LONG).show();
                }
                finally
                {
                    msgEdit.setText(null);
                }
            }
        });
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    public String getProfileEmail()
    {
        return "profileMail";
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    //--------------------------------------------------------------------------------
    // Helper functions
    //--------------------------------------------------------------------------------

}
