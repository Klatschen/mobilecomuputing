package de.erfurt.kraba.krabaerfurt.databse;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//-------------------------------------------------------------------------
// Helper class for SQLIte database
//-------------------------------------------------------------------------

public class AppDBHelper extends SQLiteOpenHelper
{
    //-------------------------------------------------------------------------
    // static members
    //-------------------------------------------------------------------------
     public static final String LOG_TAG = "DB";

    //-------------------------------------------------------------------------
    // public members
    //-------------------------------------------------------------------------
    public static final String DB_NAME = "careman.db";
    public static final int DB_VERSION = 1;

    //-------------------------------------------------------------------------
    // Chat data
    //-------------------------------------------------------------------------
    public static final String TABLE_MESSAGES = "MESSAGES";
    public static final String MESSAGES_ID = "_id";
    public static final String MESSAGES_TEXT = "MESSAGE";
    public static final String MESSAGES_FROM = "SOURCE";
    public static final String MESSAGES_AT = "AT";
    public static final String SQL_CREATE_MESSAGES =
            "CREATE TABLE " + TABLE_MESSAGES +
                    "(" + MESSAGES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MESSAGES_TEXT + " TEXT, " +
                    MESSAGES_FROM + " TEXT, " +
                    MESSAGES_AT + " datetime default current_timestamp);";
    //-------------------------------------------------------------------------
    // End of chat data
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Session data
    //-------------------------------------------------------------------------
    public static final String TABLE_SESSIONS       = "SESSIONS";
    public static final String SESSIONS_ID          = "_id";
    public static final String SESSIONS_DATE        = "DATE";
    public static final String SQL_CREATE_SESSIONS  =
            "CREATE TABLE " + TABLE_SESSIONS
            +"("
                    + SESSIONS_ID +     " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + SESSIONS_DATE +   " TEXT"
            + ");";
    //-------------------------------------------------------------------------
    // End of session data
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Block data
    //-------------------------------------------------------------------------
    public static final String TABLE_BLOCKS          = "BLOCKS";
    public static final String BLOCKS_ID             = "_id";
    public static final String BLOCKS_NAME           = "NAME";
    public static final String BLOCKS_TIMEINMIN      = "TIMEINMIN";
    public static final String BLOCKS_TYPEOFPRACTICE = "TYPEOFPRACTICE";
    public static final String BLOCKS_PRACTICES      = "PRACTICES";
    public static final String BLOCKS_SESSION_ID     = "SESSIONID";
    public static final String SQL_CREATE_BLOCKS     =
            "CREATE TABLE " + TABLE_BLOCKS
            + "("
                    + BLOCKS_ID             +   " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + BLOCKS_NAME           + " TEXT, "
                    + BLOCKS_TIMEINMIN      + " TEXT, "
                    + BLOCKS_TYPEOFPRACTICE + " TEXT, "
                    + BLOCKS_SESSION_ID     + " INTEGER, "
                    + BLOCKS_PRACTICES      + " TEXT"
            + ");";
    //-------------------------------------------------------------------------
    // End of block data
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Practices data
    //-------------------------------------------------------------------------
    public static final String TABLE_PRACTICES      = "PRACTICES";
    public static final String PRACTICES_ID         = "_id";
    public static final String PRACTICES_NAME       = "NAME";
    public static final String PRACTICES_ROUNDS     = "ROUNDS";
    public static final String PRACTICES_BLOCK_ID   = "BLOCKID";
    public static final String SQL_CREATE_PRACTICES =
            "CREATE TABLE " + TABLE_PRACTICES
            + "("
                    + PRACTICES_ID          + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + PRACTICES_NAME        + " TEXT, "
                    + PRACTICES_BLOCK_ID    + " INTEGER, "
                    + PRACTICES_ROUNDS      + " INTEGER"
            + ");";
    //-------------------------------------------------------------------------
    // End of practices data
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Override methods
    //-------------------------------------------------------------------------

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        createDataTables(db);
        createChatTables(db);
    }

    /**
     * Drops all tables on upgrade, then creates new version
     * @param db Database
     * @param oldVersion old database version
     * @param newVersion new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        deleteChatTables(db);
        deleteDataTables(db);
        onCreate(db);
    }

    //-------------------------------------------------------------------------
    // public methods
    //-------------------------------------------------------------------------

    public AppDBHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(LOG_TAG,"Die Datenbank: " + getDatabaseName() + " wurde erstellt...");
    }

    public void createDataTables(SQLiteDatabase _db)
    {
        try
        {
            _db.execSQL(SQL_CREATE_SESSIONS);
            _db.execSQL(SQL_CREATE_BLOCKS);
            _db.execSQL(SQL_CREATE_PRACTICES);
        }
        catch (Exception e)
        {
            Log.e(LOG_TAG,e.getMessage());
        }
    }

    public void createChatTables(SQLiteDatabase _db)
    {
        try
        {
            _db.execSQL(SQL_CREATE_MESSAGES);
        }
        catch (Exception e)
        {
            Log.e(LOG_TAG,e.getMessage());
        }
    }



    public void deleteChatTables(SQLiteDatabase _db)
    {
        _db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
    }

    public void deleteDataTables(SQLiteDatabase _db)
    {
        _db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSIONS);
        _db.execSQL("DROP TABLE IF EXISTS " + TABLE_BLOCKS);
        _db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRACTICES);
    }

    //-------------------------------------------------------------------------
    // helper methods
    //-------------------------------------------------------------------------
    /**
     * Drops all tables and create new empty tables
     * @param _db Reference to database
     */
    public void deleteData(SQLiteDatabase _db)
    {
        deleteChatTables(_db);
        deleteDataTables(_db);
        onCreate(_db);
    }
}
