package de.erfurt.kraba.krabaerfurt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.erfurt.kraba.krabaerfurt.datamodel.Session;

public class SessionListAdapter extends ArrayAdapter<Session>
{
    //--------------------------------------------------------------------------------
    // member variables
    //--------------------------------------------------------------------------------
    private final Context context;
    private final List<Session> itemsArrayList;

    public SessionListAdapter(Context context, List<Session> itemsArrayList)
    {
        super(context, R.layout.row_session_list, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.row_session_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.ID   = (TextView) convertView.findViewById(R.id.row_session_list_ID);
            viewHolder.Date = (TextView) convertView.findViewById(R.id.row_session_list_date);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.ID.setText(String.valueOf(itemsArrayList.get(position).getID()));
        viewHolder.Date.setText(itemsArrayList.get(position).getDate());

        return convertView;
    }

    //--------------------------------------------------------------------------------
    // helper classes
    //--------------------------------------------------------------------------------

    static class ViewHolder
    {
        public TextView ID;
        public TextView Date;
    }
}
