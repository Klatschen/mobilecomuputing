package de.erfurt.kraba.krabaerfurt;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.erfurt.kraba.krabaerfurt.databse.AppDataSource;
import de.erfurt.kraba.krabaerfurt.datamodel.Session;

public class SessionFragment extends Fragment implements AdapterView.OnItemClickListener
{

    /**
     * Method for Listener. Get ID and call 'communicate'
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        TextView c = (TextView) view.findViewById(R.id.row_session_list_ID);
        String sessionID = c.getText().toString();
        communicate(Long.valueOf(sessionID));
        Log.i("Sessions","OnClick");
    }

    /**
     * Public interface for communication between Activity and Fragment.
     * sendID takes ID, which will be needed to get content from database in other fragments
     */
    public interface Communicator
    {
        void sendID(long _id);
    }

    private Communicator m_Callback;
    private AppDataSource m_dataSource;
    private ListView m_lvSessions;
    private SessionListAdapter m_adapter;
    private TextView m_emptyText;

    @Override
    public void onResume()
    {
        super.onResume();
    }

    /**
     * Updates content.
     */
    public void updateView()
    {
        m_dataSource = new AppDataSource(getActivity());
        m_dataSource.open();

        List<Session> sessionList = m_dataSource.getAllSessions();
        m_adapter = new SessionListAdapter(getActivity(), sessionList);

        m_lvSessions.setAdapter(m_adapter);
        m_lvSessions.setEmptyView(m_emptyText);

        m_dataSource.close();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    /**
     * Activity must implement Communicator, otherwise NullException
     * @param _id Session ID
     */
    private void communicate(long _id)
    {
        m_Callback.sendID(_id);
        Log.i("Sessions","Communicate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_session, container, false);
    }

    /**
     * Initializes ListView
     * Sets Communicator Callback from Activity. Activity must implement Communicator.
     * Otherwise throws ClassCastException and NullException in 'communicate' Method
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        m_emptyText = (TextView) getActivity().findViewById(R.id.fragment_sessions_lvempty);
        m_lvSessions= (ListView) getActivity().findViewById(R.id.fragment_sessions_lvSessions);
        updateView();
        m_lvSessions.setOnItemClickListener(this);

        try
        {
            m_Callback = (Communicator) getActivity();
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(getActivity().toString()
                    + " must implement Communicator!");
        }

        ((MainActivity) getActivity()).setActionBarTitle("Sessions");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

}
