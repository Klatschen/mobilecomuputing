package de.erfurt.kraba.krabaerfurt;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class FragmentBlog extends Fragment
{
    //-------------------------------------------------------------------------
    // const members
    //-------------------------------------------------------------------------
    private static final String mc_URL_Pospis   = "https://pospislifestyle.wordpress.com/";
    private static final String mc_URL_Kraba    = "http://www.vereinfachedeintraining.com/";

    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------
    private WebView m_WebView;
    private int m_id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_blog, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity) getActivity()).setActionBarTitle("Blog");
        m_WebView = (WebView)getActivity().findViewById(R.id.frament_blog_webview);
        WebSettings webSettings = m_WebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        m_id = ((ApplicationKraba)getActivity().getApplicationContext()).GetBlogID();

        switch (m_id)
        {
            case 0:
            {
                m_WebView.loadUrl(mc_URL_Pospis);
                break;
            }
            case 1:
            {
                m_WebView.loadUrl(mc_URL_Kraba);
                break;
            }
            default:
            {
                m_WebView.loadUrl("www.ende.de");
            }
        }
    }
}
