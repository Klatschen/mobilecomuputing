package de.erfurt.kraba.krabaerfurt;

import android.app.Application;

//-------------------------------------------------------------------------
// Application class
//-------------------------------------------------------------------------

public class ApplicationKraba extends Application
{
    //-------------------------------------------------------------------------
    // private member
    //-------------------------------------------------------------------------
    /**
     * Used as ID for FragmentBlog.
     * 0 : PospisLifestyle
     * 1 : KrabaBlog
     */
    private int m_BlogID;
    /**
     * Used as communication variable to get from session to block
     */
    private int m_SessionID;

    /**
     * Used as communication variable to get from block to practices
     */
    private int m_BlockID;

    //-------------------------------------------------------------------------
    // Getter
    //-------------------------------------------------------------------------
    public int GetBlogID()
    {
        return m_BlogID;
    }

    public int GetSessionID()
    {
        return m_SessionID;
    }

    public int GetBlockID() { return m_BlockID; }

    //-------------------------------------------------------------------------
    //Setter
    //-------------------------------------------------------------------------
    public void SetBlogID(int _id)
    {
        m_BlogID = _id;
    }

    public void SetSessionID(int _id)
    {
        m_SessionID = _id;
    }

    public void SetBlockID(int _id)
    {
        m_BlockID = _id;
    }

    //-------------------------------------------------------------------------
    // public methods
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // private methods
    //-------------------------------------------------------------------------
}
