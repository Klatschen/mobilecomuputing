package de.erfurt.kraba.krabaerfurt.datamodel;

import java.util.List;

public class Block
{
    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------
    private int        m_ID;
    private List<Practice> m_Practices;
    private String         m_TimeInMin;
    private String         m_Name;
    private String         m_TypeOfPractice;
    private int        m_SessionID;


    //-------------------------------------------------------------------------
    // Getter
    //-------------------------------------------------------------------------

    public int getID()
    {
        return m_ID;
    }

    public int getSessionID()
    {
        return m_SessionID;
    }

    public List<Practice> getPractices()
    {
        return m_Practices;
    }

    public String getTimeInMin()
    {
        return m_TimeInMin;
    }

    public String getName()
    {
        return m_Name;
    }

    public String getTypeOfPractice()
    {
        return m_TypeOfPractice;
    }


    //-------------------------------------------------------------------------
    // Setter
    //-------------------------------------------------------------------------


    public void setID(int _id)
    {
        m_ID = _id;
    }

    public void setSessionID(int _id)
    {
        m_SessionID = _id;
    }

    public void setPractices(List<Practice>_practices)
    {
        m_Practices = _practices;
    }

    public void setTimeInMin(String _timeInMin)
    {
        m_TimeInMin = _timeInMin;
    }

    public void setName(String _name)
    {
        m_Name = _name;
    }

    public void setTypeOfPractice(String _typeOfPractice)
    {
        m_TypeOfPractice = _typeOfPractice;
    }
}
