package de.erfurt.kraba.krabaerfurt.datamodel;

public class Practice
{
    //-------------------------------------------------------------------------
    // private members
    //-------------------------------------------------------------------------
    private int m_ID;
    private int m_BlockID;
    private String  m_Name;
    private String  m_Rounds;

    //-------------------------------------------------------------------------
    // Getter
    //-------------------------------------------------------------------------

    public int getID()
    {
        return m_ID;
    }

    public int getBlockID()
    {
        return m_BlockID;
    }

    public String getName()
    {
        return m_Name;
    }

    public String getRounds()
    {
        return m_Rounds;
    }

    //-------------------------------------------------------------------------
    // Setter
    //-------------------------------------------------------------------------

    public void setID(int _id)
    {
        m_ID = _id;
    }

    public void setBlockID(int _id)
    {
        m_BlockID = _id;
    }

    public void setName(String _name)
    {
        m_Name = _name;
    }

    public void setRounds(String _rounds)
    {
        m_Rounds = _rounds;
    }
}
