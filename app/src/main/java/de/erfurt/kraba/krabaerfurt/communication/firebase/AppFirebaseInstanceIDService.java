package de.erfurt.kraba.krabaerfurt.communication.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class AppFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    //-------------------------------------------------------------------------
    // static members
    //-------------------------------------------------------------------------
    private static final String TAG = "Firebase";

    //-------------------------------------------------------------------------
    // override methods
    //-------------------------------------------------------------------------

    @Override
    public void onTokenRefresh()
    {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    //-------------------------------------------------------------------------
    // private methods
    //-------------------------------------------------------------------------

    private void sendRegistrationToServer(String token)
    {
        // Add custom implementation, as needed.
    }
}