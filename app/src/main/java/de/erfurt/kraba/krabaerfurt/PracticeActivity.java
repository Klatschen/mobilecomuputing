package de.erfurt.kraba.krabaerfurt;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.erfurt.kraba.krabaerfurt.databse.AppDataSource;
import de.erfurt.kraba.krabaerfurt.datamodel.Practice;

public class PracticeActivity extends AppCompatActivity
{
    //-------------------------------------------------------------------------
    // private member
    //-------------------------------------------------------------------------
    AppDataSource   m_dataSource;
    PracticeAdapter m_adapter;
    ListView        m_lvPractices;
    TextView        m_emptyText;
    int             m_blockID;


    //-------------------------------------------------------------------------
    // override methods
    //-------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        try
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (Exception ex)
        {
            Log.e("Toolbar", ex.getMessage());
        }

        m_lvPractices  = (ListView) findViewById(R.id.activity_practice_lvPractices);
        m_emptyText = (TextView) findViewById(R.id.activity_practices_lvEmpty);

        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();
            if(extras == null)
            {
                m_blockID = -99;
            }
            else
            {
                m_blockID = extras.getInt("blockID");
            }
        }
        else
        {
            m_blockID = (int) savedInstanceState.getSerializable("blockID");
        }
        updateView(m_blockID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putInt("blockID",m_blockID);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        m_blockID = savedInstanceState.getInt("blockID");
    }

    //-------------------------------------------------------------------------
    // content methods
    //-------------------------------------------------------------------------

    /**
     * Updates content.
     */
    public void updateView(int _blockID)
    {
        m_dataSource = new AppDataSource(this);
        m_dataSource.open();

        List<Practice> practiceList = m_dataSource.getAllPracticesByBlockID(_blockID);
        m_adapter = new PracticeAdapter(this, practiceList);

        m_lvPractices.setAdapter(m_adapter);
        m_lvPractices.setEmptyView(m_emptyText);

        m_dataSource.close();
    }
}

