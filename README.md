# Beschreibung #

Lars Weickert | Christoph Pospischil

App für Kraft und Bewegungsakademie Erfurt.

### Aktueller Stand der App ###

* Anzeigen der Blogs der Trainer
* Hauptmenü
* Chat vorbeireitet
* SQLite Datenbank und Tabellen sowie zugehörige Methoden erstellt
* Datenmodell erstellt (so weit wie benötigt)
* Google Cloud Messaging vorbereitet (wurde durch Firebase ersetzt)
* Testdaten können angelegt und angezeigt werden

### Info ###

Ohne Serverkomponente (MC2) ist die App noch ziemlich sinnlos. Der Trainer soll später mittels eines Web-Interfaces Trainingspläne an den User senden können (intern - XML). Diese werden dann in der aktuellen Form dargestellt und können abgehakt werden. Ein Chat ist vorgesehen und bereits halb implementiert.

Eventuell kann der Nutzer auch eigene Übungen erstellen, und zum Überprüfen an den Trainer senden.